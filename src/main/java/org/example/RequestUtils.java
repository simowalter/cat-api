package org.example;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.example.beans.CatBeans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.security.SecureRandom;

import static org.example.Main.URL_RANDOM_CAT;

public class RequestUtils {

    // private CatBeans cat ; (Suggest remove by SonarQube check)
    static SecureRandom rand = new SecureRandom();  // SecureRandom is preferred to Random

    public static CatBeans loadCatBean() throws Exception {

        String responseJson = sendGet(URL_RANDOM_CAT);
        //Parser le JSON avec le bon bean et GSON
        ArrayList<CatBeans> listCat = new Gson().fromJson(responseJson, new TypeToken<ArrayList<CatBeans>>(){}.getType());
        
        int ind = rand.nextInt(listCat.size()); // Suggestion by SonarQube check (this.rand. instead of new Random(). )
        
        return listCat.get(ind);
    }
    public static String sendGet(String url) throws Exception {
        System.out.println("url : " + url);
        OkHttpClient client = new OkHttpClient();

        //Création de la requête
        Request request = new Request.Builder().url(url).build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code " + response);
            }
            return response.body().string();
        }
        finally {
            if(response != null) {
                response.close();
            }
        }
    }
}

//https://api.thecatapi.com/v1/images/search?limit=10