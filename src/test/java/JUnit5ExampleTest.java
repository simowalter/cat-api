import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JUnit5ExampleTest {

    @Test
    void justAnExample() {
        assertTrue(true);
    }

    @Test
    void justAnExample2() {
        assertTrue(1+1==2);
    }

    @Test
    void justAnExample3() {
        assertTrue(1+2==3);
    }
}
